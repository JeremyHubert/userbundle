<?php

namespace Ifgm\UserBundle\EventListener;

use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;

class UserProfileEditListener
{
    protected $avatar;
    protected $user;
    protected $form;
    protected $uploadPath;

    public function __construct($uploadPath)
    {
        $this->uploadPath = $uploadPath;
    }

    /**
     * Load the user
     *
     * @param GetResponseUserEvent $event
     */
    public function onUserLoaded(GetResponseUserEvent $event)
    {
        $this->user = $event->getUser();
        $this->avatar = $this->user->getAvatar();
    }

    /**
     * Update avatar if file is uploaded, else remain the older one
     *
     * @param FormEvent $event
     */
    public function onFormValidated(FormEvent $event)
    {
        $this->form = $event->getForm();
        if ($this->form['avatar']->getData()) {
            $file = $this->form['avatar']->getData();
            $movedFile = $file->move($this->uploadPath, $this->user->getId().'.'.$file->getClientOriginalExtension());
            $this->user->setAvatar($movedFile->getPathname());
        } else {
            $this->user->setAvatar($this->avatar);
        }
    }
}
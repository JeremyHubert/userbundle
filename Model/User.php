<?php

namespace Ifgm\UserBundle\Model;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * @ORM\Entity()
 * @ORM\Table(name="ifgm_user")
 */
class User extends BaseUser {
    /**
     * @var string avatar
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $avatar;

    /**
     * @param string $avatar
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }

    /**
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }
}
<?php

/*
 * This file is part of the HWIOAuthBundle package.
 *
 * (c) Hardware.Info <opensource@hardware.info>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ifgm\UserBundle\Security\Core\User;

use FOS\UserBundle\Model\UserManagerInterface;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\OAuthAwareUserProviderInterface;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class providing a bridge to use the FOSUB user provider with HWIOAuth.
 *
 * In order to use the class as a connector, the appropriate setters for the
 * property mapping should be available.
 *
 * @author Alexander <iam.asm89@gmail.com>
 */
class FOSUBUserProvider implements OAuthAwareUserProviderInterface
{
    /**
     * @var UserManagerInterface
     */
    protected $userManager;

    /**
     * @var array
     */
    protected $properties;

    /**
     * @var array
     */
    protected $paths;


    /**
     * Constructor.
     *
     * @param UserManagerInterface $userManager FOSUB user provider.
     * @param array                $paths       Paths infos for image handling
     * @param array                $properties  Property mapping.
     */
    public function __construct(UserManagerInterface $userManager, array $paths, array $properties)
    {
        $this->userManager = $userManager;
        $this->properties  = $properties;
        $this->paths       = $paths;
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $username = $response->getUsername();

        // Find User by oauth account id
        $user = $this->userManager->findUserBy(array($this->getProperty($response) => $username));

        if (null === $user) {
            $user = $this->userManager->findUserBy(array('email' => $response->getEmail()));
            if (null === $user) {
                $user = $this->userManager->createUser();
                $user->setUsername($response->getNickname());
                $user->setEmail($response->getEmail());
                $user->setEnabled(true);
                $user->setLocked(false);
                $user->setExpired(false);
                $user->setCredentialsExpired(false);
            }

            $this->connect($user, $response);
        }

        $this->getAvatar($user, $response);

        return $user;
    }

    /**
     * {@inheritDoc}
     */
    public function connect($user, UserResponseInterface $response)
    {
        $property = $this->getProperty($response);
        $setter = 'set'.ucfirst($property);

        if (!method_exists($user, $setter)) {
            throw new \RuntimeException(sprintf("Class '%s' should have a method '%s'.", get_class($user), $setter));
        }

        $username = $response->getUsername();

        if (null !== $previousUser = $this->userManager->findUserBy(array($property => $username))) {
            $previousUser->$setter(null);
            $this->userManager->updateUser($previousUser);
        }

        $user->$setter($username);

        $this->userManager->updateUser($user);
    }

    /**
     * Gets the property for the response.
     *
     * @param UserResponseInterface $response
     *
     * @return string
     *
     * @throws \RuntimeException
     */
    protected function getProperty(UserResponseInterface $response)
    {
        $resourceOwnerName = $response->getResourceOwner()->getName();

        if (!isset($this->properties[$resourceOwnerName])) {
            throw new \RuntimeException(sprintf("No property defined for entity for resource owner '%s'.", $resourceOwnerName));
        }

        return $this->properties[$resourceOwnerName];
    }

    protected function getAvatar($user, UserResponseInterface $response)
    {
        $username = $response->getUsername();

        if (!$user->getAvatar() && $avatar = $response->getProfilePicture()) {
            $filename = $this->paths['temp'].'/'.$username;
            file_put_contents($filename, file_get_contents($avatar));
            $file = new File($filename);
            $movedFile = $file->move($this->paths['upload'], $username.'.'.$file->guessExtension());
            $user->setAvatar($movedFile->getPathname());
        }
    }
}